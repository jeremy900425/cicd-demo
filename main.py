# my_functions.py
def add(a, b):
    return a + b

def subtract(a, b):
    return a - b

if __name__ == '__main__':
    print(add(1, 2))
    print(add(-1, 1))
    print(subtract(2, 1))